# Based on:
# https://github.com/mmatur/traefik-cert-manager
# traefik-cert-manager.tar.zstd contains a copy from 2020-07-07
# Slight improvement for cert-manager, which now has a flag for CRDs

# Currently the official traefik helm chart uses a version <2.
# To use traefik 2:
helm repo add traefik https://containous.github.io/traefik-helm-chart

# Add the Jetstack Helm repository for cert manager
helm repo add jetstack https://charts.jetstack.io

# Update local Helm chart repository cache
helm repo update


kubectl create namespace traefik
helm install --namespace traefik traefik traefik/traefik --values values.yaml

# access to dashboard:
kubectl port-forward -n traefik $(kubectl get pods -n traefik --selector "app.kubernetes.io/name=traefik" --output=name) 9000:9000
# ⇒ http://127.0.0.1:9000/dashboard/#/

# Install cert-manager

# Create the namespace for cert-manager
kubectl create namespace cert-manager

# Install the cert-manager Helm chart
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --set installCRDs=true \
  --version v0.15.2

kubectl apply -f cluster-issuer.yaml

# See https://github.com/mmatur/traefik-cert-manager on how to create a certificate.
# (Easiest if you clone the repository and modify the values)


# When using cloudflare as a proxy see cloudflare directory.
