pip3 install --user kubectl-kadalu
PATH="$PATH:$HOME/.local/bin"
kubectl kadalu install

kubectl kadalu storage-add storage-pool-unsafe1 --type Replica1 --device pig.3.141.digital:/dev/md/3

kubectl kadalu storage-add storage-pool-safe --type Replica3 \
  --device pig.3.141.digital:/dev/md/2 \
  --device rat.3.141.digital:/dev/disk/by-partlabel/brick \
  --device zebra.3.141.digital:/dev/disk/by-partlabel/brick

kubectl apply -f storageClasses.yaml
kubectl patch storageclass kadalu -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'

