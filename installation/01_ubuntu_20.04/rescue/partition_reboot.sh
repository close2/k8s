#!/bin/sh
echo e2fsck disk before resize
e2fsck -y -f /dev/sda1

echo resize root ext4 filesystem
resize2fs /dev/sda1 20G

echo resize /dev/sda1 will require user input
parted /dev/sda resizepart 1 25GB

echo add 2nd partition for glusterfs
parted /dev/sda mkpart brick xfs 25GB 100%

echo resize root ext4 filesystem to complete /dev/sda1 size
resize2fs /dev/sda1

echo You can reboot now

