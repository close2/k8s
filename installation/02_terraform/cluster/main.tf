resource "rke_cluster" "cluster" {
  nodes {
    address = "pig.3.141.digital"
    internal_address = "5.9.65.10"
    user    = "rke"
    role    = ["controlplane", "worker", "etcd"]
    ssh_key = file("~/.ssh/pig.key")
  }
  nodes {
    address = "rat.3.141.digital"
    internal_address = "116.202.28.172"
    user    = "rke"
    role    = ["controlplane", "worker", "etcd"]
    ssh_key = file("~/.ssh/pig.key")
  }
  nodes {
    address = "zebra.3.141.digital"
    internal_address = "116.202.29.31"
    user    = "rke"
    role    = ["controlplane", "worker", "etcd"]
    ssh_key = file("~/.ssh/pig.key")
  }
  ingress {
    provider = "none"
  }
  network {
    plugin = "calico"
    options = {
        calico_flex_volume_plugin_dir = "/opt/kubernetes/kubelet-plugins/volume/exec/nodeagent~uds"
        flannel_backend_type = "vxlan"
    }
  }
  ssh_agent_auth = true
  addons_include = [
    "https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml",
    "https://gist.githubusercontent.com/superseb/499f2caa2637c404af41cfb7e5f4a938/raw/930841ac00653fdff8beca61dab9a20bb8983782/k8s-dashboard-user.yml",
  ]
}

resource "local_file" "kube_cluster_yaml" {
  filename = "${path.root}/kube_config_cluster.yml"
  sensitive_content  = rke_cluster.cluster.kube_config_yaml
}

