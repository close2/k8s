# go through main.tf and update hostnames and IPs
terraform init
terraform validate
terraform plan
terraform apply


# Backup the terraform.tfstate file(s)!

cat kube_config_cluster.yml > ~/.kube/config

#### DONE ####
# More info below


Based on:
  https://medium.com/@brotandgames/deploy-a-kubernetes-cluster-using-terraform-and-rke-provider-68112463e49d
  ( https://vitobotta.com/2019/10/14/kubernetes-hetzner-cloud-terraform-ansible-rancher/ )

* To access the dashboard:
kubectl proxy
⇒ http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login

* Get the k8s token for the dashboard:
kubectl --kubeconfig kube_config_cluster.yml -n kube-system describe secret $(kubectl --kubeconfig kube_config_cluster.yml -n kube-system get secret | grep admin-user | awk '{print $1}') | grep ^token: | awk '{ print $2 }'

Do not use hostnames as addresses!
[[ etcdmain: error verifying flags, expected IP in URL for binding ]]

Maybe:
https://github.com/rancher/rke/issues/1725
https://github.com/etcd-io/etcd/issues/9575


if apply fails with:
Failed running cluster err:Failed to get job complete status for job rke-network-plugin-deploy-job in namespace kube-system

simply run again.


Reset docker:
docker stop $(sudo docker ps -aq)
docker rm -f $(docker ps -a -q)
docker volume rm $(docker volume ls -q)
